# Simple top down shooter

## Controls

WASD - move  
E - pickup/drop items  
Mouse - look around, aim  
LMB - shoot  
