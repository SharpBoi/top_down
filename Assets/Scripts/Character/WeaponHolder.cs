﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Держатель оружия
/// </summary>
public class WeaponHolder : MonoBehaviour, IWeaponSetable {

    [Header("Hold data")]
    public WeaponBase Weapon;
    public float LerpSpeed = 10f;
    public bool HoldPosition = true;
    public bool HoldRotation = true;

    [Header("Events")]
    public PickWeaponEvent OnStartHold;
    public PickWeaponEvent OnStopHold;

    private Transform weaponPrevParent;


    void Start () { }

    private void FixedUpdate () {
        if (Weapon != null) {
            if (HoldPosition)
                Weapon.transform.localPosition = Vector3.Lerp(Weapon.transform.localPosition, Vector3.zero + Weapon.Offset, Time.fixedDeltaTime * LerpSpeed);
            if (HoldRotation)
                Weapon.transform.localRotation = Quaternion.Euler(Weapon.Angle);
        }
    }

    public void SetWeapon (WeaponBase weapon) {
        Weapon = weapon;
        weaponPrevParent = Weapon.transform.parent;
        Weapon.transform.parent = transform;
    }
    public void LoseWeapon (WeaponBase weapon) {
        Weapon.transform.parent = weaponPrevParent;
        Weapon = null;
    }

}
