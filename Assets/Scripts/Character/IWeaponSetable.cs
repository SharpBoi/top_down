﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Описывает сущность, которой можно задать задать объект оружия или убрать его
/// </summary>
interface IWeaponSetable {
    /// <summary>
    /// Задать объект оружия
    /// </summary>
    /// <param name="weapon"></param>
    void SetWeapon (WeaponBase weapon);
    /// <summary>
    /// Убрать оружие
    /// </summary>
    /// <param name="weapon"></param>
    void LoseWeapon (WeaponBase weapon);
}
