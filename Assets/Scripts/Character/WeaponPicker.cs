﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Подниматель оружия
/// </summary>
[RequireComponent(typeof(Collider))]
public class WeaponPicker : MonoBehaviour {

    public WeaponBase PickedWeapon;
    public KeyCode PickUpKey = KeyCode.E;
    public KeyCode DropKey = KeyCode.E;

    public PickWeaponEvent OnPick;
    public PickWeaponEvent OnDrop;

    private List<WeaponBase> nearbyWeapons = new List<WeaponBase>();
    private bool picked = false;

    void Start () {

    }

    private void Update () {
        if (Input.GetKeyDown(DropKey) && picked) {
            drop();
        }
        else if (Input.GetKeyDown(PickUpKey)) {
            pickup();
        }
    }

    /// <summary>
    /// Поднимает самое близкое оружие, находящееся в триггере коллайдера.
    /// </summary>
    private void pickup () {
        if (picked == false && nearbyWeapons.Count > 0) {
            WeaponBase closestWeapon = (from w in nearbyWeapons
                                        let minDist = nearbyWeapons.Min(value => (value.transform.position - transform.position).sqrMagnitude)
                                        where (w.transform.position - transform.position).sqrMagnitude == minDist
                                        select w)
                      .FirstOrDefault();

            picked = true;
            PickedWeapon = closestWeapon;
            PickedWeapon.OnPick();
            OnPick.Invoke(PickedWeapon);
        }
    }
    /// <summary>
    /// Дропает поднятое оружие
    /// </summary>
    private void drop () {
        picked = false;
        PickedWeapon.OnDrop();
        OnDrop.Invoke(PickedWeapon);
        PickedWeapon = null;
    }

    private void OnTriggerEnter (Collider other) {
        WeaponBase weapon = other.GetComponentInParent<WeaponBase>();
        if (weapon != null) {
            if (nearbyWeapons.Contains(weapon) == false) {
                nearbyWeapons.Add(weapon);
            }
        }
    }
    private void OnTriggerExit (Collider other) {
        WeaponBase weapon = other.GetComponentInParent<WeaponBase>();
        if (weapon != null) {
            nearbyWeapons.Remove(weapon);
        }
    }
}
