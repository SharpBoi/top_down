﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Получает данные о попадании снаряда в хост сущность
/// </summary>
public class ProjectileReceiver : MonoBehaviour {

    public GotShotEvent OnGotShot;

    private void Start () { }

    public void GotShot (Projectile proj, RaycastHit hit) {
        OnGotShot.Invoke(proj, hit);
    }

}
