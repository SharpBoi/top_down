﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Снаряд для стрельбы. Пулится
/// </summary>
public class Projectile : MonoBehaviour, IPoolable {
    public bool IsInPool { get; set; }

    /// <summary>
    /// Откуда летит снаряд
    /// </summary>
    public Vector3 Origin;
    /// <summary>
    /// Направление полета снаряда
    /// </summary>
    public Vector3 Dir;
    [Tooltip("Время жизни снаряда SEC")]
    public float LifeTime = 10;
    
    [Tooltip("Пулить ли снаряд по истечению времени жизни ")]
    public bool ProneTtlDisable = false;
    public float Speed;
    public float Damage;
    public TrailRenderer Trail;


    private float crntLifeTime = 0;
    private bool canFly = false;


    public void OnPool () {
        gameObject.SetActive(false);
    }
    public void OnUnpool () {
        crntLifeTime = 0;
        gameObject.SetActive(true);
        transform.position = Origin;
        ProneTtlDisable = true;
    }

    private void FixedUpdate () {
        if (IsInPool == false) {
            if (ProneTtlDisable)
                crntLifeTime += Time.fixedDeltaTime;
            if (crntLifeTime >= LifeTime) {
                ProjectilePool.Singleton.Pool(this);
            }

            if (canFly) {
                float flyDistance = Speed * Time.fixedDeltaTime;
                Vector3 prevPos = transform.position;
                transform.position += Dir * flyDistance;

                if (Physics.Raycast(prevPos, Dir, out RaycastHit hit, flyDistance)) {
                    ProjectileReceiver shootable = hit.collider.GetComponentInParent<ProjectileReceiver>();
                    if (shootable) {
                        shootable.GotShot(this, hit);
                    }
                    ProjectilePool.Singleton.Pool(this);
                }
            }
        }
    }

    /// <summary>
    /// Начать полет снаряда
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="dir"></param>
    public void StartFly (Vector3 origin, Vector3 dir) {
        Origin = origin;
        dir.Normalize();
        Dir = dir;
        transform.position = origin;
        transform.LookAt(Origin + Dir);
        canFly = true;
        if (Trail != null) {
            Trail.Clear();
        }
    }

    private void OnDrawGizmos () {
        Gizmos.DrawLine(Origin, Origin + Dir * 100);
    }

}
