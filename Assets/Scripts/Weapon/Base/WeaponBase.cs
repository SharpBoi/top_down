﻿using UnityEngine;
using System.Collections;
using System.Linq;

//todo: декомпозировать на отдельные Оружие и Пикейбл
/// <summary>
/// Базовый класс оружия
/// </summary>
public abstract class WeaponBase : MonoBehaviour {

    [Header("Body")]
    public Rigidbody Rigid;
    [Tooltip("'Отключать' ригид при пикинге")]
    public bool DisableRigidOnPick = true;
    [Tooltip("Отключать коллайдеры при пикинге")]
    public bool DisableCollidersOnPick = true;

    [Header("Ammo config")]
    public int MaxAmmo = 100;
    public int StartAmmo = 100;
    [Range(0, 100)]
    public float Damage = 1;
    public int CurrentAmmo;

    [Header("Shooting")]
    public float ShotsPerSec = 2;
    public int EmitPerShot = 5;
    [Tooltip("Разброс")]
    public float Scatter = 0;
    public bool CanShoot = true;
    [Tooltip("Позиция источника стрельбы")]
    public Vector3 ShootSource;
    [Tooltip("Направление стрельбы")]
    public Vector3 ShootAngle;

    [Header("Positioning")]
    [Tooltip("Смещение оружия в руках")]
    public Vector3 Offset;
    [Tooltip("Угловое смещение оружия в руках")]
    public Vector3 Angle;

    public event System.Action<WeaponBase> OnShot;


    private Coroutine shootHandler;

    /// <summary>
    /// Источник стрельбы в мировых координатах
    /// </summary>
    public Vector3 ShootSourceWorld => transform.TransformPoint(ShootSource);
    /// <summary>
    /// Направле4ние стрельбы в мире
    /// </summary>
    public Vector3 ShootDirWorld => transform.TransformDirection(Quaternion.Euler(ShootAngle) * Vector3.forward).normalized;
    /// <summary>
    /// Разбросанное направление стрельбы в мире
    /// </summary>
    public Vector3 ShootDirWorldScattered {
        get {
            Vector3 spread = new Vector3(
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                1);
            Vector3 dir = transform.TransformDirection(Quaternion.Euler(ShootAngle + spread * Scatter) * Vector3.forward);
            dir.Normalize();
            return dir;
        }
    }

    /// <summary>
    /// Происходит ли сейчас попытка стрельбы
    /// </summary>
    public bool IsShootingNow { get; private set; }



    void Start () {
        CurrentAmmo = StartAmmo;
    }


    protected abstract void onBeforeShoot ();
    protected abstract void onAfterShoot ();
    protected abstract void onShoot ();

    public void OnPick () {
        if (DisableRigidOnPick)
            Rigid.isKinematic = true;
        if (DisableCollidersOnPick)
            foreach (var coll in GetComponentsInChildren<Collider>()) {
                coll.enabled = false;
            }
    }
    public void OnDrop () {
        if (DisableRigidOnPick)
            Rigid.isKinematic = false;
        if (DisableCollidersOnPick)
            foreach (var coll in GetComponentsInChildren<Collider>()) {
                coll.enabled = true;
            }
    }

    

    public void StartShooting () {
        if (shootHandler == null) {
            IsShootingNow = true;
            onBeforeShoot();
            shootHandler = StartCoroutine(shoot());
        }
    }
    public void StopShooting () {
        if (shootHandler != null) {
            IsShootingNow = false;
            StopCoroutine(shootHandler);
            shootHandler = null;
            onAfterShoot();
        }
    }

    private IEnumerator shoot () {
        float sec = 1;
        while (CanShoot && IsShootingNow) {
            sec += Time.deltaTime * ShotsPerSec;
            if (sec >= 1) {
                sec = 0;
                if (CurrentAmmo > 0) {
                    onShoot();
                    CurrentAmmo--;
                    OnShot?.Invoke(this);
                }
            }

            yield return 0;
        }
    }

    private void OnDrawGizmos () {
        Gizmos.DrawLine(ShootSourceWorld, ShootSourceWorld + ShootDirWorld);
    }
}
