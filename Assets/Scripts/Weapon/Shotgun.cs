﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Дробовик
/// </summary>
public class Shotgun : WeaponBase {    

    protected override void onAfterShoot () {
    }

    protected override void onBeforeShoot () {
    }

    protected override void onShoot () {
        for (int i = 0; i < EmitPerShot; i++) {
            Projectile proj = ProjectilePool.Singleton.Get();
            proj.Damage = Damage;
            proj.StartFly(ShootSourceWorld, ShootDirWorldScattered);
        }
    }
}
