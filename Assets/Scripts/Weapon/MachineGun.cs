﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Автоматическое оружие
/// </summary>
public class MachineGun : WeaponBase {

    protected override void onAfterShoot () {
    }

    protected override void onBeforeShoot () {
    }

    protected override void onShoot () {
        Projectile proj = ProjectilePool.Singleton.Get();
        proj.Damage = Damage;
        proj.StartFly(base.ShootSourceWorld, base.ShootDirWorldScattered);
    }
}
