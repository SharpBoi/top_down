﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Описывает возможность получать урон
/// </summary>
public class Damageable : MonoBehaviour {
    public static List<Damageable> Damageables = new List<Damageable>();


    public float Health = 100;

    public UnityEvent OnHealthIsOver;


    private void Start () {
        Damageables.Add(this);
    }
    private void OnDestroy () {
        Damageables.Remove(this);
    }


    public void TakeShotDamage (Projectile proj, RaycastHit hit) {
        applyDamage(proj.Damage);
    }

    private void applyDamage (float damage) {
        if (Health > 0) {

            Health -= Mathf.Round(damage * 10f) / 10;
            Health = Mathf.Clamp(Health, 0, 100);

            if (Health.Eq(0f, 0.001f)) {
                OnHealthIsOver.Invoke();
            }
        }
    }

    private void OnDrawGizmos () {
#if UNITY_EDITOR
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.black;
        style.fontSize = 20;
        Handles.Label(transform.position, Health.ToString(), style);
#endif
    }
}
