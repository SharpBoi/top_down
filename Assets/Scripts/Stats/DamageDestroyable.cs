﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Описывает возможность быть уничтоженным при получении урона
/// </summary>
[RequireComponent(typeof(Damageable))]
public class DamageDestroyable : MonoBehaviour {

    private Damageable damageable;


    void Start () {
        damageable = GetComponent<Damageable>();
        damageable.OnHealthIsOver.AddListener(doDestroy);
    }

    void doDestroy() {
        Destroy(gameObject);
    }
}
