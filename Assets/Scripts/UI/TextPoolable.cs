﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Пулейбл обертка над текстом
/// </summary>
public class TextPoolable : Text, IPoolable {
    public bool IsInPool { get; set; }

    public void OnPool () {
        gameObject.SetActive(false);
    }

    public void OnUnpool () {
        gameObject.SetActive(true);
    }
}
