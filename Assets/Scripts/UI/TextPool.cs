﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Пул для ЮИ текстов
/// </summary>
public class TextPool : PoolBase<TextPoolable> {
    public static TextPool Singleton;

    [Tooltip("Префаб текста")]
    public TextPoolable TextAsset;
    /// <summary>
    /// Трансформ в котоырй анпулить объекты
    /// </summary>
    public Transform UnpoolTransform;

    protected override TextPoolable create () {
        return Instantiate(TextAsset.gameObject).GetComponent<TextPoolable>();
    }

    protected override void createSingleton () {
        if (Singleton) throw new System.Exception("its a singleton");
        Singleton = this;
    }

    protected override void prepool () {
        for (int i = 0; i < Prepool; i++)
            base.Pool(create());
    }

    // Use this for initialization
    void Start () {
        base.unpoolTransform = UnpoolTransform;
        createSingleton();
        prepool();
    }
}
