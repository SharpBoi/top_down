﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Визуализатор данных об игроке
/// </summary>
public class UI_PlayerStateVizualizator : MonoBehaviour {

    [Header("Data")]
    public PlayerState PlayerState;

    [Header("Vizualization")]
    public TextMeshProUGUI Kills;
    public TextMeshProUGUI Health;
    public TextMeshProUGUI Ammo;


    void Start () {
        PlayerState.OnKillsChanged += PlayerState_OnKillsChanged;
        PlayerState.OnAmmoChanged += PlayerState_OnAmmoChanged;
    }

    private void PlayerState_OnAmmoChanged () {
        Ammo.text = PlayerState.Ammo.ToString();
    }

    private void PlayerState_OnKillsChanged () {
        Kills.text = PlayerState.Kills.ToString();
    }
}
