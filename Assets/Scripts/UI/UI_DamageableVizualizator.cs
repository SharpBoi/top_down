﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Визуализирует данные всех Damageable обхектов
/// </summary>
class UI_DamageableVizualizator : MonoBehaviour {

    [Tooltip("Камера, к которое подцеплен ЮИ")]
    public Camera Cam;
    [Tooltip("Дистанция от камеры до текста, на которой начнется фейдинг текста")]
    public float TextFadeDistance = 20;
    [Tooltip("Кривая фейда")]
    public AnimationCurve TextFadeCurve;


    private List<TextPoolable> unpoled = new List<TextPoolable>();

    private void FixedUpdate () {
        // пулим все лишние тексты
        int difference = unpoled.Count - Damageable.Damageables.Count;
        for (int i = 0; i < difference; i++) {
            TextPoolable text = unpoled[0];
            unpoled.RemoveAt(0);
            TextPool.Singleton.Pool(text);
        }
        // анпулим если недостает
        difference = Damageable.Damageables.Count - unpoled.Count;
        for (int i = 0; i < difference; i++)
            unpoled.Add(TextPool.Singleton.Get());

        // позим тексты, передаем данные, управляем фейдом
        for (int i = 0; i < Damageable.Damageables.Count; i++) {
            unpoled[i].text = Damageable.Damageables[i].Health.ToString();

            unpoled[i].transform.position = Cam.WorldToScreenPoint(Damageable.Damageables[i].transform.position);
            Color c = unpoled[i].color;
            c.a = (1f - TextFadeCurve.Evaluate((TextFadeDistance * TextFadeDistance) / (Cam.transform.position - Damageable.Damageables[i].transform.position).sqrMagnitude));
            unpoled[i].color = c;
        }
    }
}
