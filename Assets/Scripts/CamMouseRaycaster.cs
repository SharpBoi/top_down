﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Рейкастит курсор мыши в мир, и передает данные всем MouseRaycastReceiver
/// </summary>
[RequireComponent(typeof(Camera))]
public class CamMouseRaycaster : MonoBehaviour {

    #region STATICS
    private static List<MouseRaycastReceiver> receivers = new List<MouseRaycastReceiver>();
    public static void AddReceiver (MouseRaycastReceiver receiver) {
        receivers.Add(receiver);
    }
    public static void RemoveReceiver (MouseRaycastReceiver receiver) {
        receivers.Remove(receiver);
    }
    #endregion


    public Camera Cam;
    private RaycastHit hit;
    private bool rayHittingNow;
    private Collider prevCollider;
    private bool prevHitState;


    void Start () {

    }

    // Update is called once per frame
    void Update () {
        Vector3 mouseWorld = Input.mousePosition;
        mouseWorld.z = 1;
        mouseWorld = Cam.ScreenToWorldPoint(mouseWorld);

        rayHittingNow = Physics.Raycast(transform.position, (mouseWorld - transform.position).normalized, out hit, float.MaxValue);

        for (int i = 0; i < receivers.Count; i++) {
            receivers[i].UpdateHitInfo(hit, rayHittingNow);
        }
    }

    private void OnDrawGizmos () {
        Gizmos.DrawSphere(hit.point, 0.4f);
    }
}
