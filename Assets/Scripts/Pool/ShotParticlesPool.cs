﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Пул для частиц при попадании
/// </summary>
public class ShotParticlesPool : PoolBase<ShotParticles> {
    public static ShotParticlesPool Singleton;

    [Tooltip("Префаб частиц")]
    public ShotParticles ParticlesAsset;

    protected override ShotParticles create () {
        return Instantiate(ParticlesAsset);
    }

    protected override void createSingleton () {
        if (Singleton != null) throw new System.Exception("Its a singleton");
        Singleton = this;
    }

    protected override void prepool () {
        for (int i = 0; i < Prepool; i++) {
            base.Pool(Instantiate(ParticlesAsset));
        }
    }

    void Start () {
        createSingleton();
        prepool();
    }
}
