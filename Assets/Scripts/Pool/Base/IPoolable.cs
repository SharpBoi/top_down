﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Описывает пулируемый объект
/// </summary>
public interface IPoolable {
    bool IsInPool { get; set; }

    /// <summary>
    /// Зажигается при пулинге
    /// </summary>
    void OnPool ();
    /// <summary>
    /// Зажигается при анпулинге
    /// </summary>
    void OnUnpool ();
}
