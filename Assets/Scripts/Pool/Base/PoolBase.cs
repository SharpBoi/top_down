﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Базовый пулер. Описывает общее поведение пула
/// </summary>
/// <typeparam name="Tobj"></typeparam>
public abstract class PoolBase<Tobj> : MonoBehaviour, IPool<Tobj> where Tobj : MonoBehaviour, IPoolable {

    [Tooltip("Сколько инстансов заготовить в начале игры")]
    public int Prepool = 0;


    Stack<GameObject> pooled = new Stack<GameObject>();

    protected abstract void createSingleton ();

    protected abstract void prepool ();

    protected abstract Tobj create ();

    protected Transform unpoolTransform = null;

    /// <summary>
    /// Получает объект из пула. Если есть свободный - вернет его из стэка. Если свободных нет - создаст новый и вернет его(при этом размер стэка растет)
    /// </summary>
    /// <returns></returns>
    public Tobj Get () {
        Tobj res;
        if (pooled.Count == 0) {
            res = create();
        }
        else {
            res = pooled.Pop().GetComponent<Tobj>();
        }


        (res as IPoolable).OnUnpool();
        (res as IPoolable).IsInPool = false;
        //res.transform.parent = null;
        res.transform.SetParent(unpoolTransform);
        return res;
    }

    /// <summary>
    /// Пулит объект обратно в стэк
    /// </summary>
    /// <param name="obj"></param>
    public void Pool (Tobj obj) {
        (obj as IPoolable).OnPool();
        (obj as IPoolable).IsInPool = true;
        //obj.transform.parent = transform;
        obj.transform.SetParent(transform);
        pooled.Push(obj.gameObject);
    }

    /// <summary>
    /// СОдержит ли пул данный объект
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public bool Contains (Tobj obj) {
        return pooled.Contains(obj.gameObject);
    }
}
