﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Описывает пул объектов
/// </summary>
/// <typeparam name="Tobj">Тип пулируемого объекта</typeparam>
public interface IPool<Tobj> where Tobj : IPoolable {

    Tobj Get ();
    void Pool (Tobj obj);

    bool Contains (Tobj obj);

}
