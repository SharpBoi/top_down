﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Пул для снарядов
/// </summary>
public class ProjectilePool : PoolBase<Projectile> {
    public static ProjectilePool Singleton;

    [Tooltip("Префаб снаряда")]
    public Projectile ProjectileAsset;

    private void Start () {
        createSingleton();
        prepool();
    }

    protected override void createSingleton () {
        if (Singleton != null) throw new System.Exception("Its a singleton");
        Singleton = this;
    }

    protected override void prepool () {
        for (int i = 0; i < Prepool; i++) {
            base.Pool(Instantiate(ProjectileAsset));
        }
    }

    protected override Projectile create () {
        return Instantiate(ProjectileAsset);
    }
}
