﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Обертка для частиц выстрела. Пулейбл. Анпулит частицы в точке попадания и ожидает смерти всех частицы, затем, пулится обратно
/// </summary>
public class ShotParticles : MonoBehaviour, IPoolable {

    public ParticleSystem Particles;
    public bool PoolWhenTimeEnd = true;

    public bool IsInPool { get; set; }

    public void OnPool () {
        Particles.Stop();
        Particles.time = 0;
        gameObject.SetActive(false);
    }

    public void OnUnpool () {
        Particles.time = 0;
        gameObject.SetActive(true);
    }

    public void Play () {
        Particles.Play();
        StartCoroutine(awaitParticles());
    }

    /// <summary>
    /// Задает цвет частиц
    /// </summary>
    /// <param name="c">собсна цвет</param>
    /// <param name="spread">разброс V в HSV параметра</param>
    public void SetColor (Color c, float spread) {
        ParticleSystem.MainModule main = Particles.main;

        Gradient grad = new Gradient();
        grad.mode = GradientMode.Blend;

        Color.RGBToHSV(c, out float h, out float s, out float v);
        float leftv = Mathf.Clamp01(0.5f - spread / 2);
        float rightv = Mathf.Clamp01(0.5f + spread / 2);

        Color leftc = Color.HSVToRGB(h, s, leftv);
        Color rightc = Color.HSVToRGB(h, s, rightv);

        grad.SetKeys(
        new GradientColorKey[] {
            new GradientColorKey(leftc, 0),
            new GradientColorKey(rightc, 1)
        },
        new GradientAlphaKey[] {
            new GradientAlphaKey(1, 0),
            new GradientAlphaKey(1, 1)
        });

        main.startColor = new ParticleSystem.MinMaxGradient(grad);
    }

    private IEnumerator awaitParticles () {
        while (true) {

            if (Particles.isPlaying == false) {
                ShotParticlesPool.Singleton.Pool(this);
                break;
            }

            yield return 0;
        }
    }
}
