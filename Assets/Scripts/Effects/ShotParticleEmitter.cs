﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Создает частицы от попадания выстрела
/// </summary>
[RequireComponent(typeof(ProjectileReceiver))]
public class ShotParticleEmitter : MonoBehaviour {

    public Color ParticlesColor;
    public float ColorValueSpread = 0;



    private ProjectileReceiver receiver;

    private void Start () {
        receiver = GetComponent<ProjectileReceiver>();
        receiver.OnGotShot.AddListener(Emit);
    }
    private void OnDestroy () {
        receiver.OnGotShot.RemoveListener(Emit);
    }

    /// <summary>
    /// Собсна создание частиц
    /// </summary>
    /// <param name="proj"></param>
    /// <param name="hit"></param>
    private void Emit (Projectile proj, RaycastHit hit) {
        ShotParticles particles = ShotParticlesPool.Singleton.Get();
        particles.transform.position = hit.point;
        particles.transform.LookAt(hit.normal);
        particles.SetColor(ParticlesColor, ColorValueSpread);
        particles.Play();
    }
}
