﻿using UnityEngine;
using System.Collections;
using UnityEditor;

/// <summary>
/// Применяет силу от выстрела к хост объекту
/// </summary>
[RequireComponent(typeof(ProjectileReceiver))]
public class ShotForceApplier : MonoBehaviour {

    public Rigidbody Rigid;
    public float ForceMutiplier = 1;


    private ProjectileReceiver receiver;


    private void Start () {
        receiver = GetComponent<ProjectileReceiver>();
        receiver.OnGotShot.AddListener(ApplyForce);
    }
    private void OnDestroy () {
        receiver.OnGotShot.RemoveListener(ApplyForce);
    }

    /// <summary>
    /// собсна применение силы
    /// </summary>
    /// <param name="proj"></param>
    /// <param name="hit"></param>
    private void ApplyForce (Projectile proj, RaycastHit hit) {
        Rigid.AddForceAtPosition(proj.Dir.normalized * ForceMutiplier, hit.point, ForceMode.Impulse);
    }
}
