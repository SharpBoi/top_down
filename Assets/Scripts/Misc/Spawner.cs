﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Спавнит рандомный объект из данных, в рандомной позиции в указанной области
/// </summary>
public class Spawner : MonoBehaviour {

    public List<GameObject> Assets = new List<GameObject>();
    public Vector3 SpawnArea;
    public int MaxInstances = 10;
    public bool CanSpawn = true;


    private List<SpawnerAgent> spawned = new List<SpawnerAgent>();


    private Vector3 scaledSpawnArea => Vector3.Scale(SpawnArea, transform.lossyScale);


    private void OnDestroy () {
        for(int i = 0; i < spawned.Count; i++) {
            Destroy(spawned[i]);
        }
        spawned.Clear();
    }

    /// <summary>
    /// Спавнит объект со SpawnerAgent компонентом
    /// </summary>
    public void SpawnWithAgent () {
        if (spawned.Count < MaxInstances) {
            SpawnerAgent agent = addSpawnerAgent(spawn());
            if (spawned.Contains(agent) == false) {
                spawned.Add(agent);
            }
        }
    }


    /// <summary>
    /// Спавн в области
    /// </summary>
    /// <returns></returns>
    private GameObject spawn () {
        Vector3 spawnPoint = new Vector3();
        spawnPoint.x = Random.Range(-scaledSpawnArea.x, scaledSpawnArea.x);
        spawnPoint.y = Random.Range(-scaledSpawnArea.y, scaledSpawnArea.y);
        spawnPoint.z = Random.Range(-scaledSpawnArea.z, scaledSpawnArea.z);
        spawnPoint = transform.TransformPoint(spawnPoint);

        GameObject inst = Instantiate(Assets[Mathf.RoundToInt(Random.value * (Assets.Count - 1))]);
        inst.transform.position = spawnPoint;

        return inst;
    }
    /// <summary>
    /// Добавление агента
    /// </summary>
    /// <param name="inst"></param>
    /// <returns></returns>
    private SpawnerAgent addSpawnerAgent (GameObject inst) {
        SpawnerAgent agent = inst.GetOrAddComponent<SpawnerAgent>();
        agent.OnEntityDestroy += handleAgentDestroy;
        return agent;
    }

    /// <summary>
    /// получение события дестроя из спавнер агента
    /// </summary>
    /// <param name="agent"></param>
    private void handleAgentDestroy (SpawnerAgent agent) {
        spawned.Remove(agent);
    }

    public void OnDrawGizmos () {
        Matrix4x4 defaultMat = Gizmos.matrix;

        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        Gizmos.DrawWireCube(Vector3.zero, SpawnArea * 2);

        Gizmos.matrix = defaultMat;
    }
}
