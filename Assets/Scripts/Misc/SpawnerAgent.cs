﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Агент заспавненного объекта. Передает некоторые события в Spawner
/// </summary>
public class SpawnerAgent : MonoBehaviour {
    /// <summary>
    /// Если сущность/агент дестройнуты
    /// </summary>
    public event Action<SpawnerAgent> OnEntityDestroy;

    private void OnDestroy () {
        OnEntityDestroy?.Invoke(this);
    }
}
