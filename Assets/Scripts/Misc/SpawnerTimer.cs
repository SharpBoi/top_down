﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Управляет справном во времени
/// </summary>
[RequireComponent(typeof(Spawner))]
public class SpawnerTimer : MonoBehaviour {

    public float SpawnsPerSec = 1;
    public bool CanTick = true;


    private Spawner spawner;
    private float spawnTime = 0;


    void Start () {
        spawner = GetComponent<Spawner>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (CanTick) {
            spawnTime += Time.fixedDeltaTime * SpawnsPerSec;
            if (spawnTime >= 1) {
                spawnTime = 0;

                spawner.SpawnWithAgent();
            }
        }
    }
}
