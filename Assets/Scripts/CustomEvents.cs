﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[System.Serializable]
public class PickWeaponEvent : UnityEvent<WeaponBase> { };

[System.Serializable]
public class GotShotEvent : UnityEvent<Projectile, RaycastHit> { };