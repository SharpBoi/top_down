﻿using UnityEngine;
using System.Collections;

public static class GameObjectExtensions {
    /// <summary>
    /// Возвращает компонент данного типа. Если такового нет - создает и возвращает
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="host"></param>
    /// <returns></returns>
    public static T GetOrAddComponent<T> (this GameObject host) where T : Component {
        T comp = host.GetComponent<T>();
        if (comp != null) return comp;

        

        return host.AddComponent<T>();
    }
}
