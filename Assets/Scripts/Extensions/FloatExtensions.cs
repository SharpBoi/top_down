﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class FloatExtensions {
    public static bool Eq(this float host, float other, float epsilon) {
        return Mathf.Abs(host - other) <= epsilon;
    }
}
