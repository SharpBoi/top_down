﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Стейт игрока
/// </summary>
[CreateAssetMenuAttribute(fileName = "PlayerStateAsset", menuName = "Create player state", order = 0)]
public class PlayerState : ScriptableObject {
    [NonSerialized]
    public float Health = 100;
    [NonSerialized]
    public int Kills = 0;
    [NonSerialized]
    public int Ammo = 0;


    public event Action OnKillsChanged;
    public event Action OnAmmoChanged;

    public void AddKill (int count) {
        Kills += count;
        OnKillsChanged?.Invoke();
    }
    public void AddOneKill () {
        AddKill(1);
    }
    /// <summary>
    /// задает текущее оружие
    /// </summary>
    /// <param name="weapon"></param>
    public void SetCurrentWeapon (WeaponBase weapon) {
        handleWeaponAmmoUpdate(weapon);
        weapon.OnShot += handleWeaponAmmoUpdate;
    }
    /// <summary>
    /// ремувит текущее оружие из стейта
    /// </summary>
    public void LoseWeapon () {
        Ammo = 0;
        OnAmmoChanged?.Invoke();
    }
    /// <summary>
    /// Управляет получением данных об оружии и зажиганием события при их изменении
    /// </summary>
    /// <param name="weapon"></param>
    private void handleWeaponAmmoUpdate (WeaponBase weapon) {
        Ammo = weapon.CurrentAmmo;
        OnAmmoChanged?.Invoke();
    }
}


[CustomEditor(typeof(PlayerState))]
public class PlayerStateInspector : Editor {

    public override void OnInspectorGUI () {
        base.OnInspectorGUI();

        PlayerState tgt = target as PlayerState;

        GUILayout.Label($"Kills: {tgt.Kills}");
        GUILayout.Label($"Health: {tgt.Health}");
    }
}