﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Получает данные от MouseRaycaster и фильтрует их
/// </summary>
public class MouseRaycastReceiver : MonoBehaviour {

    #region FIELDS
    [Tooltip("Выбирать только объекты в маске")]
    public LayerMask ChooseMask = 0b01;
    [Tooltip("Выбирать только объекты с тагом. Если любой таг - *")]
    public string[] ChooseTagMask = new string[1] {"*"};

    public RaycastHit HitInfo;
    public bool HittingNowFiltered;
    public bool HittingNowAny;
    #endregion

    void Start () {
        CamMouseRaycaster.AddReceiver(this);
    }

    private void OnDestroy () {
        CamMouseRaycaster.RemoveReceiver(this);
    }

    public void UpdateHitInfo (RaycastHit hit, bool isHititngNow) {
        HitInfo = hit;
        HittingNowFiltered = isHititngNow;
        HittingNowAny = isHititngNow;

        if (isHititngNow) {
            if (ChooseMask != (ChooseMask | (1 << hit.collider.gameObject.layer))) {
                HittingNowFiltered = false;
            }
            if (Enumerable.Contains(ChooseTagMask, hit.collider.gameObject.tag) == false && ChooseTagMask[0] != "*") {
                HittingNowFiltered = false;
            }
        }

        Debug.Log(HittingNowFiltered);
    }
}
