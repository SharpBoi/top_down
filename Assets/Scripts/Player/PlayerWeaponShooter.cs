﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Управляет стрельбой удерживаемого в данный момент оружия
/// </summary>
public class PlayerWeaponShooter : MonoBehaviour, IWeaponSetable {

    [Header("Object")]
    public WeaponBase Weapon;

    [Header("Controls")]
    public string FireAxisName = "Fire1";


    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        if (Weapon != null) {
            if (Input.GetAxis(FireAxisName).Eq(1, 0.001f) && Weapon.IsShootingNow == false) {
                Weapon.StartShooting();
            }
            else if (Input.GetAxis(FireAxisName).Eq(0, 0.001f) && Weapon.IsShootingNow) {
                Weapon.StopShooting();
            }
        }
    }
    public void SetWeapon (WeaponBase weapon) {
        Weapon = weapon;
    }

    public void LoseWeapon (WeaponBase weapon) {
        Weapon.StopShooting();
        Weapon = null;
    }

}
