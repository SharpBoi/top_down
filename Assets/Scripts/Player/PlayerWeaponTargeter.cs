﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Управляет направлением удерживаемого в данный момент оружия
/// </summary>
[RequireComponent(typeof(MouseRaycastReceiver))]
public class PlayerWeaponTargeter : MonoBehaviour {

    [Tooltip("Держатель оружия, который будет таргетитьсяs")]
    public WeaponHolder Holder;


    private MouseRaycastReceiver mouseReceiver;

    void Start () {
        mouseReceiver = GetComponent<MouseRaycastReceiver>();
    }

    private void FixedUpdate () {
        if (Holder.Weapon != null) {
            if (mouseReceiver.HittingNowFiltered) {
                Holder.HoldRotation = false;
                Holder.Weapon.transform.LookAt(mouseReceiver.HitInfo.transform);
            }
            else if (mouseReceiver.HittingNowAny) {
                Holder.HoldRotation = false;
                Holder.Weapon.transform.LookAt(mouseReceiver.HitInfo.point, Vector3.up);
            }
            else {
                Holder.HoldRotation = true;
            }
        }
    }
}
