﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Управляет перемещением игрока
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour {

    #region FIELDS
    [Header("Observation")]
    public Camera Camera;
    public Vector3 PosOffset;
    public Vector3 AngleOffset;
    public float MaxPosOffsetDistnace;
    public float OffsetSpeed = 1f;

    [Header("Movement")]
    public KeyCode Forward = KeyCode.W;
    public KeyCode Backward = KeyCode.S;
    public KeyCode Right = KeyCode.D;
    public KeyCode Left = KeyCode.A;

    public float MaxSpeed = 1f;
    [Range(0, 1)]
    public float SpeedFalloff = 0.8f;


    Vector3 rsltMove = new Vector3();
    Rigidbody rigid;
    bool grounded = false;
    #endregion


    void Start () {
        rigid = GetComponent<Rigidbody>();
    }

    private void FixedUpdate () {
        updateCameraPosition();
        updateMovement();
    }

    void updateCameraPosition () {
        Vector3 offsetWorld = transform.position + (PosOffset);
        Camera.transform.position = Vector3.Lerp(
            Camera.transform.position,
            offsetWorld + (rsltMove).normalized * MaxPosOffsetDistnace, Time.fixedDeltaTime * OffsetSpeed);
        Camera.transform.rotation = Quaternion.Euler(AngleOffset);
    }

    void updateMovement () {
        rsltMove.Set(0, 0, 0);

        if (Input.GetKey(Forward)) {
            rsltMove += transform.forward;
        }
        if (Input.GetKey(Backward)) {
            rsltMove += -transform.forward;
        }
        if (Input.GetKey(Right)) {
            rsltMove += transform.right;
        }
        if (Input.GetKey(Left)) {
            rsltMove += -transform.right;
        }

        rigid.AddForce(rsltMove, ForceMode.VelocityChange);

        Vector3 horVel = rigid.velocity;
        if (horVel.magnitude > MaxSpeed) {
            horVel.Normalize();
            horVel *= MaxSpeed;
            Vector3 vel = rigid.velocity;
            vel.x = horVel.x;
            vel.z = horVel.z;
            rigid.velocity = vel;
        }
        if (grounded) {
            Vector3 vel = rigid.velocity;
            vel.x *= (1 - SpeedFalloff);
            vel.z *= (1 - SpeedFalloff);
            rigid.velocity = vel;
        }
    }

    private void OnCollisionStay (Collision collision) {
        grounded = true;
    }
    private void OnCollisionExit (Collision collision) {
        grounded = false;
    }

    private void OnDrawGizmos () {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);

        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, new Vector3(Camera.transform.position.x, transform.position.y, Camera.transform.position.z));
        Gizmos.DrawLine(new Vector3(Camera.transform.position.x, transform.position.y, Camera.transform.position.z), Camera.transform.position);
    }
}
