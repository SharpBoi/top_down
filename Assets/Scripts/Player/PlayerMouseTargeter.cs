﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Управляет направлением игрока. Берет данные из MouseRaycastReceiver
/// </summary>
[RequireComponent(typeof(MouseRaycastReceiver))]
public class PlayerMouseTargeter : MonoBehaviour {

    #region FIELDS
    public Camera Cam;


    private MouseRaycastReceiver raycastReceiver;
    #endregion

    void Start () {
        raycastReceiver = GetComponent<MouseRaycastReceiver>();
    }

    private void FixedUpdate () {
        Vector3 dir = Vector3.zero;
        if (raycastReceiver.HittingNowFiltered) {
            if (raycastReceiver.HitInfo.transform)
                dir = (raycastReceiver.HitInfo.transform.position - transform.position);
        }
        else {
            Vector3 playerPosScreenCentrized = Cam.WorldToScreenPoint(transform.position);
            playerPosScreenCentrized.x -= Screen.width / 2;
            playerPosScreenCentrized.y -= Screen.height / 2;

            Vector3 mouseScreenCentrized = Input.mousePosition;
            mouseScreenCentrized.x -= Screen.width / 2;
            mouseScreenCentrized.y -= Screen.height / 2;

            //dir = (raycastReceiver.HitInfo.point - transform.position);
            dir = mouseScreenCentrized - playerPosScreenCentrized;
            dir.z = dir.y;

            Debug.Log(playerPosScreenCentrized);
        }
        dir.Normalize();

        float deg = -Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg + 90f;

        transform.rotation = Quaternion.Euler(0, deg, 0);
    }
}
