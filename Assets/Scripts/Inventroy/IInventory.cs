﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IInventory {
    List<IItem> Items { get; }
    void PushItem (IItem item);
    IItem RemoveItem ( );
}
